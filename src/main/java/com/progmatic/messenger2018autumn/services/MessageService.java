/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.services;

import com.progmatic.messenger2018autumn.domain.Message;
import com.progmatic.messenger2018autumn.helpers.JPQLParamCondition;
import com.progmatic.messenger2018autumn.helpers.JPQLParamHelper;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

/**
 *
 * @author peti
 */
@Service
public class MessageService {
    
    @PersistenceContext
    EntityManager em;

    
    public List<Message> findMessages(
            Long id, String author, String text,
            LocalDateTime from, LocalDateTime to,
            String orderBy, Boolean asc){
        
        JPQLParamHelper ph = new JPQLParamHelper();
        String sqlBase = "select m from Message m ";
        ph.addParam("m.text", text);
        ph.addParam("m.author", author);
        ph.addParam("m.createDate", JPQLParamCondition.GTE, from);
        ph.addParam("m.createDate", JPQLParamCondition.LTE, to);
        Query q = em.createQuery(sqlBase + ph.creteWhereCondition());
        ph.addParams(q);
        return q.getResultList();
    }
    
    public Message findMessageById(Long id){
        return em.find(Message.class, id);
    }
    
    @PreAuthorize("hasAuthority('ADMIN')")
    @Transactional
    public boolean deleteMessage(Long id){
        Message m = em.find(Message.class, id);
        if(m==null){
            return false;
        }
        em.remove(m);
        return true;
    }
    
    @Transactional
    public void createMessage(Message m){
        em.persist(m);
    }
}
